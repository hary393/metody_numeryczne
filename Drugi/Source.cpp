#include<iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>
using namespace std;
#define rozm 27					//rozmiar planszy
#define rozmkw rozm*rozm		//rozmiar planszy kwadrat potrzebny do oblicze�

struct blok{
	int gracz=0;
	int poz1;
	int poz2;
	blok *wartosc=NULL;
};
struct bloklist{
	int gracz = 0;
	int poz1;
	int poz2;
	bloklist *next = NULL;
}head;


void monte(int *plansza, int przebieg){		//Wielokrotne losowe generowanie przebiegu gry 
	double wygrana = 0;
	double gracz1 = 0;
	for (int k = 0; k < przebieg; k++){
		int poz1 = 0;
		int poz2 = 0;
		while (true){
			poz1 += (rand() % 6) + 1;
			if (poz1 >= rozm){
				gracz1++;
				break;
			}
			poz1 += plansza[poz1];
			poz2 += (rand() % 6) + 1;
			if (poz2 >= rozm){
				break;
			}
			poz2 += plansza[poz2];
		}
	}
	wygrana = gracz1 / przebieg;
	cout << setprecision(20) << fixed <<"Monte Carlo "<< wygrana * 100 << "  %" << endl;
}


void generator(blok r�wnania[rozmkw*2], int *plansza){		//generator r�wna�
	for (int x = 0; x < rozmkw * 2; x++){
		r�wnania[x].wartosc = new blok[7];		// mo�e by� max 6 sk�adowych (ko�� 6 scienna) + wyraz wolny // jakie r�wnania potrzebujuemy do wyniku s� tu wpisywane
		for (int k = 0; k < 6; k++){			//wszystkie rzuty ko�ci�    je�eli potrzebnych rozwi�za� by�o mniej ni� 6 to pozosta�e maj� ustawione gracz==0
			if (r�wnania[x].gracz == 1){
				
				if (r�wnania[x].poz1 + k + 1 >= rozm){		//je�eli po rzucie wygra�
					r�wnania[x].wartosc[6].gracz++;
				}
				else if (plansza[r�wnania[x].poz1 + k + 1] >= 0){      //je�eli nie trafi� w pu�apk� 
					r�wnania[x].wartosc[k].poz1 = r�wnania[x].poz1 + k + 1;
					r�wnania[x].wartosc[k].gracz = 2;
					r�wnania[x].wartosc[k].poz2 = r�wnania[x].poz2;
				}
				else{			//je�eli trafi� w pu�apk� 
					r�wnania[x].wartosc[k].poz1 = r�wnania[x].poz1 + k + 1 + (plansza[r�wnania[x].poz1 + k + 1]);//o ile si� cofn�� 
					r�wnania[x].wartosc[k].gracz = 2;
					r�wnania[x].wartosc[k].poz2 = r�wnania[x].poz2;
				}
			}
			else{
				if (r�wnania[x].poz2 + k + 1 >= rozm){   //je�eli gracz 2 wygra� 
				}
				else if (plansza[r�wnania[x].poz2 + k + 1] >= 0){		//je�eli nie trafi� w pu�apk� 
					r�wnania[x].wartosc[k].poz2 = r�wnania[x].poz2 + k + 1;
					r�wnania[x].wartosc[k].gracz = 1;
					r�wnania[x].wartosc[k].poz1 = r�wnania[x].poz1;
				}
				else{   //je�eli trafi� w pu�apk� 
					r�wnania[x].wartosc[k].poz2 = r�wnania[x].poz2 + k + 1 + (plansza[r�wnania[x].poz2 + k + 1]);
					r�wnania[x].wartosc[k].gracz = 1;
					r�wnania[x].wartosc[k].poz1 = r�wnania[x].poz1;
				}
			}
		}
	}
}

int pozycja(int poz1, int poz2, int gracz){		//funkcja do wyliczania pozycji r�wnania w tablicy r�wna�
	int gracze;
	if (gracz == 1){
		gracze = 0;
	}
	else gracze = rozmkw;
	bool flag = 0;
	int pozrow =poz1 + poz2*rozm + gracze;
	return pozrow;
}


void listaM(blok *rownania,bloklist *ptr,int *rozmMa){		//Lista wyszukuj�ca potrzebne r�wnania do rozwi�zania
	bloklist *wsk=NULL;
	bloklist *nowy = NULL;
	bool flag = 0;
	do{

		int pozrow = pozycja(ptr->poz1, ptr->poz2, ptr->gracz);			//wyliczenie na kt�rej pozycji w macierzy r�wna� znajduj� si�potrzebne wyniki
		for (int k = 0; k < 6; k++){							//sprawdzenie wszystkich r�wna� odpowiedzi 
			if (rownania[pozrow].wartosc[k].gracz != 0){				//je�eli by�o ich mniej ni� 6 to reszta by�a wypisywana zerami
				wsk = &head;
				flag = 0;
				while (wsk != NULL){					
					if (rownania[pozrow].wartosc[k].poz1 == wsk->poz1 &&		//iterujemy po li�cie potrzebnych rozwi�za� sprawdaj�c czy ju� nie ma takiego r�wnania
						rownania[pozrow].wartosc[k].poz2 == wsk->poz2 &&
						rownania[pozrow].wartosc[k].gracz == wsk->gracz){
						flag = 1;
					}

					if (flag)break;
					wsk = wsk->next;
				}
				if (!flag){						//je�eli nie ma to dodajemy do listy
					wsk = &head;
					while (wsk->next != NULL){
						wsk = wsk->next;
					}
					nowy = new bloklist;
					nowy->gracz = rownania[pozrow].wartosc[k].gracz;
					nowy->poz1 = rownania[pozrow].wartosc[k].poz1;
					nowy->poz2 = rownania[pozrow].wartosc[k].poz2;
					nowy->next = NULL;
					wsk->next = nowy;
					*rozmMa+=1;					// i zwi�kszamy licznik element�w w li�cie
				}
			}
		}
		ptr = ptr->next;				//jednocze�nie iterujemy po tej samej li�cie do kt�rej doklejamy potrzebne r�wnania do rozwi�za�
	} while (ptr != NULL);		//dzi�ki czemu mamy wszystkie potrzebne r�wnania
}

void WypMacierz(double **Macierz, bloklist *ptr,blok *rowna, int rozmMa){		//wiedz�c ju� ile potrzeba r�wna� do rozwi�zania 
	bloklist *wsk;													//wype�niamy teraz macierz sk�adowymi 
	int tmp;														//sk�adowe s� wpisywane na podstawie ich kolejno�ci w li�cie 
	for (int j = 0; j < rozmMa; j++){
		int pozrow = pozycja(ptr->poz1 , ptr->poz2 , ptr->gracz);
		for (int f = 0; f < 6; f++){
			if (rowna[pozrow].wartosc[f].gracz != 0){
				tmp = 0;
				wsk = &head;
				while (rowna[pozrow].wartosc[f].poz1 != wsk->poz1 ||	//dlatego ci�gle po niej iterujemy sprawdzaj�c gdzie dana sk�adowa si� znajdowa�a 
					rowna[pozrow].wartosc[f].poz2 != wsk->poz2 ||
					rowna[pozrow].wartosc[f].gracz != wsk->gracz){
					tmp++;												//licznik kolejno�ci wyst�pienia
					wsk = wsk->next;
				}
				Macierz[j][tmp] += 1;
			}
		}
		Macierz[j][rozmMa] -= (double)rowna[pozrow].wartosc[6].gracz;		//wpisanie wyrazu wolnego
		Macierz[j][j] = -6;
		ptr = ptr->next;
	}
}


bool gauss(double ** MacRown, double * X, int rozmM)
{
	double eps = 1e-12; //przybli�enie zera
	double m = 0;
	double s = 0;

	for (int i = 0; i < rozmM - 1; i++){			// eliminacja wsp�czynnik�w
		for (int j = i + 1; j < rozmM; j++){
			if (fabs(MacRown[i][i]) < eps) { // albo == 0  sprawdzanie czy przek�tna nie jest zerem 
				return false;
			}
			m = -MacRown[j][i] / MacRown[i][i];
			for (int k = i + 1; k <= rozmM; k++){
				MacRown[j][k] += m * MacRown[i][k];
			}
		}
	}

	for (int i = rozmM - 1; i >= 0; i--){			// wyliczanie od ko�ca niewiadomych
		s = MacRown[i][rozmM];
		for (int j = rozmM - 1; j >= i + 1; j--){
			s -= MacRown[i][j] * X[j];
		}
		if (fabs(MacRown[i][i]) < eps){    // albo == 0   sprawdzanie czy przek�tna nie jest zerem 
			return false;
		}
		X[i] = s / MacRown[i][i];
	}
	return true;
}

void Seidel(double **Macierz, int rozmMa, int iteracje){		//Metoda Gaussa - Seidela
	//tworzenie wszystkich potrzebnych macierzy 
	double *X = new double[rozmMa];		 //Wynik�w
	double *B = new double[rozmMa];		//Wyraz�w Wolnych
	double **L;							//Macierzy tr�jk�tnej dolnej
	double **D;							//Macierzy diagonalnej
	double **U;							//Macierzy tr�jk�tnej g�rnej
	L= new double*[rozmMa];
	D = new double*[rozmMa];
	U = new double*[rozmMa];
	for (int x = 0; x < rozmMa; x++){
		X[x] = 0;
		B[x] = Macierz[x][rozmMa];
		L[x] = new double[rozmMa];
		D[x] = new double[rozmMa];
		U[x] = new double[rozmMa];
	}


	for (int i = 0; i < rozmMa; i++){			//Wype�nienie Macierzy tr�jk�tnej dolnej/g�rnej i diagonalnej					
		for (int j = 0; j < rozmMa; j++) {
			if (i < j) {
				U[i][j] = Macierz[i][j];
			}
			else if (i > j) {
				L[i][j] = Macierz[i][j];
			}
			else {
				D[i][j] = Macierz[i][j];
			}
		}
	}
	
	for (int i = 0; i<rozmMa; i++)  //  Obliczenie macierzy odwrotnej  1/D  
		D[i][i] = 1 / D[i][i];

	for (int i = 0; i<rozmMa; i++)   //  Obliczenie macierzy  1/D * b
		B[i] = B[i] * D[i][i];

	for (int i = 0; i < rozmMa; i++){  // Obliczenie macierzy   1/D * L
		for (int j = 0; j < i; j++)
			L[i][j] = D[i][i] * L[i][j];
	}

	for (int i = 0; i < rozmMa; i++){  //   Obliczenie macierzy  1/D * U
		for (int j = i + 1; j < rozmMa; j++)
			U[i][j] = U[i][j] * D[i][i];
	}

	for (int k = 0; k < iteracje; k++){
		for (int i = 0; i < rozmMa; i++) {
			X[i] = B[i];                       // x = 1/D   *b 
			for (int j = i + 1; j < rozmMa; j++)
				X[i] -= U[i][j] * X[j];    //   1/D   *U * x
			for (int j = 0; j < i; j++)
				X[i] -= L[i][j] * X[j];    //  odj�cie 1/D  *L * x 
		}
	}
	cout <<"Gauss - Seidel "<< X[0]*100 <<" %" << endl; // wypisanie odpowiedzi

	for (int l = 0; l < rozmMa; l++){
		delete[] L[l];
		delete[] D[l];
		delete[] U[l];
	}
	delete[] L;
	delete[] D;
	delete[] U;
	delete[] B;
	delete[] X;
}



int main(){
	srand(time(NULL));
	int plansza[rozm] = {};
	plansza[4] = -2;
	plansza[5] = -2;
	plansza[7] = -5;
	plansza[9] = -3;
	plansza[14] = -12;
	plansza[15] = -2;
	plansza[17] = -7;
	plansza[19] = -8;
	plansza[21] = -3;
	plansza[22] = -16;
	plansza[25] = -9;


	blok blok[rozmkw*2] = {};
	int poz1 = 0;
	int gracz = 1;
	int poz2 = 0;

	head.gracz = 1;		//ustawienie pocz�tku listy na element kt�ry chcemy wyliczy�
	head.poz1 = 0;
	head.poz2 = 0;

	bloklist *ptr;
	ptr = &head;

	int rozmMa = 1;
	double **Macierz;
	double **Macierz2;

	for (int x = 0; x < rozmkw * 2; x++){  //wype�nienie tablicy r�wna� mo�liwymi stanami gry 
		blok[x].gracz = gracz;
		blok[x].poz1 = poz1;
		blok[x].poz2 = poz2;
		poz1++;
		if (poz1 == rozm){
			poz1 = 0;
			poz2++;
		}
		if (poz2 == rozm){
			gracz++;
			poz1 = 0;
			poz2 = 0;
		}
	}

	generator(blok, plansza); //wygeneruj r�wnania dla ka�dego stanu gry

	listaM(blok,ptr,&rozmMa); //dodaj do listy r�wnania potrzebne do obliczenia 001

	Macierz = new double*[rozmMa];    //przydzielenie pami�ci macierzy kt�ra ma rozmiar d�ugo�ci listy
	Macierz2 = new double*[rozmMa];  // i drugiej kt�ra zostanie kopi� potrzebn� do Metody Gaussa - Seidela
	for (int l = 0; l < rozmMa; l++){    
		Macierz[l] = new double[rozmMa+1];
		Macierz2[l] = new double[rozmMa + 1];
		for (int v = 0; v < rozmMa + 1; v++){
			Macierz[l][v] = 0;
		}
	}
	ptr = &head;
	WypMacierz(Macierz, ptr, blok , rozmMa);   //wype�nienie danej macierzy warto�ciami r�wna� 

	for (int t = 0; t < rozmMa; t++){			//kopia Macierz do Macierz2
		for (int r = 0; r < rozmMa + 1; r++){
			Macierz2[t][r] = Macierz[t][r];
		}
	}

	double *Wyniki=new double[rozmMa];   //tablica wynik�w 


	if (gauss(Macierz, Wyniki, rozmMa)){			//wywo�anie rozwi�zania macierzy metod� Gaussa
		cout << setprecision(20) << fixed << "Gauss " << Wyniki[0] * 100 << " %" << endl;	// i wypisanie rozwi�zania
	}

	int iteracje;
	cout << "liczba iteracji" << endl;
	cin >> iteracje;


	Seidel(Macierz2 , rozmMa, iteracje); //wywo�anie Metody Gaussa - Seidela
	monte(plansza, 1000); //wywo�anie monte carlo
	monte(plansza, 10000); //wywo�anie monte carlo
	monte(plansza, 100000); //wywo�anie monte carlo
	//monte(plansza, 1000000); //wywo�anie monte carlo
	//monte(plansza, 100000000); //wywo�anie monte carlo




	for (int l = 0; l < rozmMa; l++){		//zwalnianie pami�ci 
		delete[] Macierz[l];
		delete[] Macierz2[l];

	}

	delete[] Macierz;   
	delete[] Macierz2;
	delete[] Wyniki;


	return 1;
}