#include<iostream>
#include<cmath>
#include <iomanip>
using namespace std;
#define M_PI 3.14159265358979323846

long long unsigned int silnia(int a){
	if (a < 2)
		return 1;
	else return a*silnia(a - 1);
}

///////////////////////WIELOMIAN TAYLORA

void DoubleTaylor(int ile,  double co,  double *gdzie){ //obliczanie wielomianem taylora
	for (int k = 0; k <= ile; k++){
		gdzie[k] = (pow(-1, k)*pow(co, 2 * k + 1)) / silnia(2 * k + 1);
	}
}

void FloatTaylor(int ile,  float co, float *gdzie){
	for (int k = 0; k <= ile; k++){
		gdzie[k] = (pow(-1, k)*pow(co, 2 * k + 1)) / silnia(2 * k + 1);
	}
}

float SumaOdPoczFloat(int ile, float co){
	float wynik = 0;
	float *tab = new float[ile + 1];
	FloatTaylor(ile, co, tab);
	for (int j = 0; j <= ile; j++){
		wynik += tab[j];
	}
	delete[] tab;
	return wynik;
}

float SumaOdKoncaFloat(int ile, float co){
	float wynik = 0;
	float *tab = new float[ile + 1];
	FloatTaylor(ile, co, tab);
	for (int j = ile; j >= 0; j--){
		wynik += tab[j];
	}
	delete[] tab;
	return wynik;
}


double SumaOdPoczDouble(int ile, double co){
	double wynik=0;
	double *tab = new double[ile + 1];
	DoubleTaylor(ile, co, tab);
	for (int j = 0; j <= ile; j++){
		wynik += tab[j];
	}
	delete[] tab;
	return wynik;
}

double SumaOdKoncaDouble(int ile,  double co){
	double wynik = 0;
	double *tab = new double[ile + 1];
	DoubleTaylor(ile, co, tab);
	for (int j = ile; j >= 0; j--){
		wynik += tab[j];
	}
	delete[] tab;
	return wynik;
}



////////////////////////////////POPRZEDNI WYRAZ



void DoubleKrok(int ile, double co, double *gdzie){   // obliczanie na podstawie poprzedniego wyrazu
	double N0 = (pow(-1, 0)*pow(co, 2 * 0 + 1)) / silnia(2 * 0 + 1);
	double poprzedni = N0;
	double nast;
	gdzie[0] = N0;
	for (int k = 1; k <= ile; k++){
		nast = ((poprzedni * pow(co, 2) / (4 * k*k + 2 * k))); // q=an/an-1   x^2/4*n^2+2*n
		gdzie[k] = pow(-1, k)*nast;
		poprzedni = nast;
	}
}


void FloatKrok(int ile, float co, float *gdzie){
	float N0 = (pow(-1, 0)*pow(co, 2 * 0 + 1)) / silnia(2 * 0 + 1);
	float poprzedni = N0;
	float nast;
	gdzie[0] = N0;
	for (int k = 1; k <= ile; k++){
		nast = ((poprzedni * pow(co, 2) / (4 * k*k + 2 * k)));
		gdzie[k] = pow(-1, k)*nast;
		poprzedni = nast;
	}
}


float KrokOdKoncaFloat(int ile, float co){
	float wynik = 0;
	float *tab = new float[ile + 1];
	FloatKrok(ile, co, tab);
	for (int j = ile; j >= 0; j--){
		wynik += tab[j];
	}
	delete[] tab;
	return wynik;
}

float KrokOdPoczFloat(int ile, float co){
	float wynik = 0;
	float *tab = new float[ile + 1];
	FloatKrok(ile, co, tab);
	for (int j = 0; j <= ile; j++){
		wynik += tab[j];
	}
	delete[] tab;
	return wynik;
}


double KrokOdKoncaDouble(int ile, double co){
	double wynik = 0;
	double *tab = new double[ile + 1];
	DoubleKrok(ile, co, tab);
	for (int j = ile; j >= 0; j--){
		wynik += tab[j];
	}
	delete[] tab;
	return wynik;
}

double KrokOdPoczDouble(int ile, double co){
	double wynik = 0;
	double *tab = new double[ile + 1];
	DoubleKrok(ile, co, tab);
	for (int j = 0; j <= ile; j++){
		wynik += tab[j];
	}
	delete[] tab;
	return wynik;
}


int main(){
	 double kat =2;
	 float katf = 2;
	 double wynik;
	 double pomiar = 0;
	 double blad = 0;
	 float pomiar2 = 0;
	 float blad2 = 0;



	wynik = sin(kat);
	cout << setprecision(40) << fixed << wynik << endl;

	//FLOAT
	cout <<"Float Krokiem od Poczatku"<< endl;
		for (int x = 1; x <= 30; x++){
			pomiar2 = KrokOdPoczFloat(x, katf);
			cout << pomiar2 << endl;						//Najpierw wypisanie pomiaru
			blad2 = (wynik - pomiar2) / wynik;
			cout << blad2 << endl<<"**************"<<endl;  //Nast�pnie b��du wzgl�dnego na podstawie funkcji bibliotecznej 
		}
		cout << endl << endl;
		cout << "Float Krokiem od Konca" << endl;
		for (int x = 1; x <= 30; x++){
			pomiar2 = KrokOdKoncaFloat(x, katf);
			cout << pomiar2 << endl;
			blad2 = (wynik - pomiar2) / wynik;
			cout << blad2 << endl << "**************" << endl;
		}
		cout << endl<<endl;
		cout << "Float Suma od Poczatku" << endl;
		for (int x = 1; x <= 30; x++){
			pomiar2 = SumaOdPoczFloat(x, katf);
			cout << pomiar2 << endl;
			blad2 = (wynik - pomiar2) / wynik;
			cout << blad2 << endl << "**************" << endl;
		}
		cout << endl << endl;
		cout << "Float Suma od Konca" << endl;
		for (int x = 1; x <= 30; x++){
			pomiar2 = SumaOdKoncaFloat(x, katf);
			cout << pomiar2 << endl;
			blad2 = (wynik - pomiar2) / wynik;
			cout << blad2 << endl << "**************" << endl;
		}
		cout << endl << endl;
		//DOUBLE
		cout << "Double Suma od Poczatku" << endl;
		for (int x = 1; x <= 30; x++){
			pomiar = SumaOdPoczDouble(x, kat);
			cout << pomiar << endl;
			blad = (wynik - pomiar) / wynik;
			cout << blad << endl << "**************" << endl;
		}
		cout << endl << endl;
		cout << "Double Suma od Konca" << endl;
		for (int x = 1; x <= 30; x++){
			pomiar = SumaOdKoncaDouble(x, kat);
			cout << pomiar << endl;
			blad = (wynik - pomiar) / wynik;
			cout << blad << endl << "**************" << endl;
		}
		cout << endl << endl;
		cout << "Double Krokiem od Poczatku" << endl;
		for (int x = 1; x <= 30; x++){
			pomiar = KrokOdPoczDouble(x, kat);
			cout << pomiar << endl;
			blad = (wynik - pomiar) / wynik;
			cout << blad << endl << "**************" << endl;
		}
		cout << endl << endl;
		cout << "Double Krokiem od Konca" << endl;
		for (int x = 1; x <= 30; x++){
			pomiar = KrokOdKoncaDouble(x, kat);
			cout << pomiar << endl;
			blad = (wynik - pomiar) / wynik;
			cout << blad << endl << "**************" << endl;
		}
		cout << endl << endl;

	return 0;
}