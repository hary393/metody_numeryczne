#include <iostream>
#include <iomanip>
using namespace std;



bool gauss(double  MacRown[20][21], double * X, int rozmM)
{
	double eps = 1e-12; //przybli�enie zera
	double m = 0;
	double s = 0;

	for (int i = 0; i < rozmM - 1; i++){			// eliminacja wsp�czynnik�w
		for (int j = i + 1; j < rozmM; j++){
			if (fabs(MacRown[i][i]) < eps) { // albo == 0  sprawdzanie czy przek�tna nie jest zerem 
				return false;
			}
			m = -MacRown[j][i] / MacRown[i][i];
			for (int k = i + 1; k <= rozmM; k++){
				MacRown[j][k] += m * MacRown[i][k];
			}
		}
	}

	for (int i = rozmM - 1; i >= 0; i--){			// wyliczanie od ko�ca niewiadomych
		s = MacRown[i][rozmM];
		for (int j = rozmM - 1; j >= i + 1; j--){
			s -= MacRown[i][j] * X[j];
		}
		if (fabs(MacRown[i][i]) < eps){    // albo == 0   sprawdzanie czy przek�tna nie jest zerem 
			return false;
		}
		X[i] = s / MacRown[i][i];
	}
	return true;
}


int main(){


	double freq[20] = { 2.160913, 2.184642, 2.208656, 2.232956, 2.257543, 2.282417, 2.307579, 2.333029,
		2.358767, 2.384794, 2.411110, 2.437714, 2.464608, 2.491789, 2.519259, 2.547017, 2.575062,
		2.603393, 2.632010, 2.660913 };

	double s21[20] = {
		0.0154473160, 0.0182357086, 0.0194462133, 0.0118513804, 0.0414972492, 0.4124997372,
		0.9972000658, 0.9942401537, 0.9938543943, 0.9845163563, 0.9975239226, 0.9947690590, 0.9844258408,
		0.9994965668, 0.8073331021, 0.4250811874, 0.2196173998, 0.1257758825, 0.0785875430, 0.0524021994 };
	double h[20] = {};
	double mi[20] = {};
	double lambda[20] = {};
	double delta[20] = {};
	double M[20][21] = {};
	double MWynik[20] = {};

	double a[20] = {};
	double b[20] = {};
	double c[20] = {};
	double d[20] = {};
	double roz=freq[19]-freq[0];
	double skok=roz/101;		//ustalenie co ile przesuwa si� cz�stotliwo�� do wyliczenia
	double curfreq=freq[0];
	double freqlicz[101];
	double fliczone[101];

	for (int x = 0; x < 20-1; x++){
		h[x+1] = freq[x + 1] - freq[x];		//wyliczenie h odleg�o�ci mi�dzy pkt
	}
	for (int x = 1; x < 20 - 1; x++){			// wyliczenie mi lamdy i delty
		mi[x] = h[x] / (h[x] + h[x + 1]);
		lambda[x] = h[x + 1] / (h[x] + h[x + 1]);
		delta[x] = (6 / (h[x] + h[x + 1]))*(((s21[x + 1] - s21[x]) / h[x + 1]) - ((s21[x] - s21[x - 1]) / h[x]));
	}
	for (int x = 1; x < 20 - 1; x++){ // wype�nienie macierzy wyrazami 
		M[x][x-1] = mi[x];
		M[x][x] = 2;
		M[x][x+1] = lambda[x];
		M[x][20] = delta[x];
	}
	M[0][0] = 2;	//wype�nienie warunk�w brzegowych
	M[0][1] = 0;
	M[0][20] = 0;
	M[19][18] = 0;
	M[19][19] = 2;
	M[19][20] = 0;

	for (int x = 0; x < 20; x++){
		for (int y = 0; y <= 20 ; y++){
			cout<<M[x][y]<<" ";
		}
		cout << endl;
	}


	if (gauss(M, MWynik, 20)){			//wywo�anie rozwi�zania macierzy metod� Gaussa
		for (int y = 0; y < 20; y++){
			cout << MWynik[y] << endl;
		}
	}

	for (int y = 0; y < 20-1; y++){			//wyliczenie wsp�czynnik�w  a b c d
		a[y] =s21[y] ;
		b[y] = ((s21[y+1]-s21[y])/h[y+1])-(((2*MWynik[y]+MWynik[y+1])/6)*h[y+1]);
		c[y] = MWynik[y]/2 ;
		d[y] = (MWynik[y+1]-MWynik[y])/(6*h[y+1]);
	}
	int node = 0;
	for (int v = 0; v < 101; v++){			//wyliczenie cz�stotliwo�ci i warfto�ci funkcji dla 101 element�w
		if (curfreq > freq[node + 1]){
			node++;
		}
		freqlicz[v]=curfreq;
		fliczone[v] = a[node] + b[node] * (curfreq - freq[node]) + c[node] * pow(curfreq - freq[node], 2) + d[node] * pow(curfreq - freq[node], 3);
		curfreq += skok;
	}
	for (int v = 0; v < 101; v++){
		cout << setprecision(20) << fixed << freqlicz[v] << endl;		//wypisanie cz�stotliwo�ci
	}

	cout << endl;

	for (int v = 0; v < 101; v++){
		cout << setprecision(20) << fixed << fliczone[v] << endl;		//wypisanie warto�ci 
	}

	return 0;
}